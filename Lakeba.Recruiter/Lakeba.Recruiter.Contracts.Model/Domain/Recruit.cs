﻿using Lakeba.Recruiter.Contracts.Model.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.Model.Domain
{
    public class Recruit : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? PositionId { get; set; }
        public string CurriculumPath { get; set; }

        public virtual Position Position { get; set; }
    }
}
