﻿using Lakeba.Recruiter.Contracts.Model.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.Model.Domain
{
    public class Position : Entity
    {
        public string Desc { get; set; }
    }
}
