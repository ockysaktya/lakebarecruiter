﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Framework.Membership
{
    public class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Nickname { get; set; }
        public string AvatarImg { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public int RoleEnum { get; set; }
    }
}
