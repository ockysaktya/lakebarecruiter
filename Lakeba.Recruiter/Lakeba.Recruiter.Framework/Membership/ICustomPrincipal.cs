﻿using Lakeba.Recruiter.Framework.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Framework.Membership
{
    public interface ICustomPrincipal : IPrincipal
    {
        int UserId { get; set; }
        string FullName { get; set; }
        string Nickname { get; set; }
        string AvatarImg { get; set; }
        string UserName { get; set; }
        string RoleName { get; set; }
        UserRoles RoleEnum { get; set; }
    }
}
