﻿using Lakeba.Recruiter.Framework.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Framework.Membership
{
    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            return role.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase);
        }

        public CustomPrincipal(string email)
        {
            this.Identity = new GenericIdentity(email);
        }

        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Nickname { get; set; }
        public string AvatarImg { get; set; }
        public string RoleName { get; set; }
        public UserRoles RoleEnum { get; set; }
        public string UserName { get; set; }
    }
}
