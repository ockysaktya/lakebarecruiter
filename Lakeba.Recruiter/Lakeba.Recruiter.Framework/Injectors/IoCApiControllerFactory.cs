﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace Lakeba.Recruiter.Framework.Injectors
{
    public class IoCApiControllerFactory : IHttpControllerActivator
    {
        readonly IWindsorContainer _container;

        public IoCApiControllerFactory(IWindsorContainer container)
        {
            _container = container;
        }
        
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            return (IHttpController)_container.Resolve(controllerType);
        }

    }
}
