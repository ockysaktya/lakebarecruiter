﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Lakeba.Recruiter.Framework.Injectors
{
    public class IoCControllerFactory : DefaultControllerFactory
    {
        readonly IWindsorContainer _container;

        public IoCControllerFactory(IWindsorContainer container)
        {
            _container = container;
            var controllerTypes =
                from t in Assembly.GetExecutingAssembly().GetTypes()
                where typeof(IController).IsAssignableFrom(t)
                select t;
            foreach (var t in controllerTypes)
                container.Register(Component.For(t).LifeStyle.Transient);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, "Page Not Found");
            }
            var controller = (IController)_container.Resolve(controllerType);
            if (controller == null)
            {
                throw new HttpException(404, "Page Not Found");
            }
            return controller;
        }
    }
}
