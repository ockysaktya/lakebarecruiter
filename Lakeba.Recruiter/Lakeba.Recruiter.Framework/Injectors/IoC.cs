﻿using Castle.Windsor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Framework.Injectors
{
    public static class IoC
    {
        private static readonly object LockObj = new object();

        private static IWindsorContainer _container = new WindsorContainer();

        public static IWindsorContainer Container
        {
            get { return _container; }

            set
            {
                lock (LockObj)
                {
                    _container = value;
                }
            }
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static T Resolve<T>(IDictionary args)
        {
            return _container.Resolve<T>(args);
        }

        public static T Resolve<T>(object anonym)
        {
            return _container.Resolve<T>(anonym);
        }

        public static object Resolve(Type type)
        {
            return _container.Resolve(type);
        }
    }
}
