﻿using System.Web.Http.Controllers;
using Castle.MicroKernel.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Lakeba.Recruiter.Framework.Injectors
{
    public class WindsorRegistrator
    {
        public static void RegisterSingleton(Type interfaceType, Type implementationType)
        {
            IoC.Container.Register(
                Component
                .For(interfaceType)
                .ImplementedBy(implementationType)
                .LifeStyle.Singleton);
        }

        public static void Register(Type interfaceType, Type implementationType)
        {
            IoC.Container.Register(
                Component
                .For(interfaceType)
                .ImplementedBy(implementationType)
                .LifeStyle.PerWebRequest);
        }

        public static void RegisterAllFromAssemblies(string assemblyName)
        {
            IoC.Container.Register(
                Classes
                .FromAssemblyNamed(assemblyName)
                .Pick()
                .WithServiceAllInterfaces()
                .LifestylePerWebRequest());
        }

        public static void RegisterThreadAllFromAssemblies(string assemblyName)
        {
            IoC.Container.Register(
                Classes
                .FromAssemblyNamed(assemblyName)
                .Pick()
                .WithServiceAllInterfaces()
                .LifestylePerThread());
        }

        public static void RegisterController(string assemblyName)
        {
            IoC.Container.Register(
                Classes
                .FromAssemblyNamed(assemblyName)
                .BasedOn<IController>()
                .LifestyleTransient()
                );
        }

        public static void RegisterApiController(string assemblyName)
        {
            IoC.Container.Register(
                Classes
                .FromAssemblyNamed(assemblyName)
                .BasedOn<IHttpController>()
                .LifestyleTransient()
                );
        }
    }
}
