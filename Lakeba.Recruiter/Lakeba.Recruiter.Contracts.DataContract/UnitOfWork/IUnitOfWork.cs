﻿using Lakeba.Recruiter.Contracts.DataContract.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.DataContract.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Save();
        void Dispose(bool disposing);
        void Dispose();
        IRepository<T> Repository<T>() where T : class, new();
    }
}
