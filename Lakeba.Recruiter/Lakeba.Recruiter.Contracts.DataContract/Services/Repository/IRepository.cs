﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.DataContract.Services.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
        TEntity Single(Expression<Func<TEntity, bool>> predicate);
        TEntity First(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        void Attach(TEntity entity);
        void Update(TEntity entity, int id);
        IEnumerable<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path);
        IEnumerable<TEntity> Include(string path);
        int Truncate();
        void Remove(TEntity entity);
    }
}
