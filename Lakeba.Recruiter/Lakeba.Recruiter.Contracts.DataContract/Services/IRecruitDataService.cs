﻿using Lakeba.Recruiter.Contracts.Model.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.DataContract.Services
{
    public interface IRecruitDataService : IDataService<Recruit>
    {
    }
}
