﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.DataContract.Services
{
    public interface IDataService<TEntity> where TEntity : class , new()
    {
        IEnumerable<TEntity> DbSet();
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
        TEntity Single(Expression<Func<TEntity, bool>> predicate);
        TEntity First(Expression<Func<TEntity, bool>> predicate);

        IEnumerable<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path);
        IEnumerable<TEntity> Include(string path);

        void Add(TEntity entity);
        void Attach(TEntity entity);
        void Update(TEntity entity, int id);
        void Delete(TEntity entity);

        void Save();

        int Truncate();
    }
}
