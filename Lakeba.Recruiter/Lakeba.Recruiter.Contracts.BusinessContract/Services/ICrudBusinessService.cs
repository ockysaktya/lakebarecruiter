﻿using Lakeba.Recruiter.Contracts.Model.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.BusinessContract.Services
{
    public interface ICrudBusinessService<T> where T : Entity, new()
    {
        T Get(int id);
        T Get(Expression<Func<T, bool>> func);
        IEnumerable<T> GetAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> func, bool showDeleted = false);

        int Add(T item);
        int Update(T item);
        void Save();
        void Delete(int id);
        void HardDelete(int id);
        void Restore(int id);

        int Truncate();
    }
}
