﻿using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Contracts.BusinessContract.ModelMapper
{
    public class IgnoreDefaultValue : LoopValueInjection
    {
        protected override bool AllowSetValue(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is DateTime && ((DateTime)value) == default(DateTime))
            {
                return false;
            }

            if (value is int && ((int)value) == default(int))
            {
                return false;
            }

            if (value is string && (string.IsNullOrEmpty(value.ToString())))
            {
                return false;
            }

            return base.AllowSetValue(value);
        }
    }
}
