﻿using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Omu.ValueInjecter;

namespace Lakeba.Recruiter.Contracts.BusinessContract.ModelMapper
{
    public class Mapper<TEntity, TInput> : IMapper<TEntity, TInput>
        where TEntity : class, new()
        where TInput : new()
    {
        public virtual TInput MapToInput(TEntity entity)
        {
            var input = new TInput();
            input.InjectFrom(entity)
                .InjectFrom<NormalToNullabels>(entity)
                .InjectFrom<EntitiesToInts>(entity);
            return input;
        }

        public virtual TEntity MapToEntity(TInput input, TEntity e)
        {
            e.InjectFrom<IgnoreDefaultValue>(input)
               .InjectFrom<IntsToEntities>(input)
               .InjectFrom<NullablesToNormal>(input);
            return e;
        }
    }
}
