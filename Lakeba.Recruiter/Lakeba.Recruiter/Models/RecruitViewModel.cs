﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lakeba.Recruiter.Models
{
    public class RecruitViewModel : BaseEntityModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Position { get; set; }
        public string CurriculumPath { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
}