﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lakeba.Recruiter.Models
{
    public class BaseEntityModel
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}