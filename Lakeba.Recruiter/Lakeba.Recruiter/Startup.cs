﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lakeba.Recruiter.Startup))]
namespace Lakeba.Recruiter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
