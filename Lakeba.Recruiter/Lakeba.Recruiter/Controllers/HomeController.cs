﻿using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Lakeba.Recruiter.Framework.Injectors;
using Lakeba.Recruiter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lakeba.Recruiter.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IRecruitBusinessService _recruitService;
        private readonly IMapper<Recruit, RecruitViewModel> _recruitMapper;

        public HomeController()
        {
            _recruitService = IoC.Resolve<IRecruitBusinessService>();
            _recruitMapper = IoC.Resolve<IMapper<Recruit, RecruitViewModel>>();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Recruit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Recruit(RecruitViewModel input)
        {
            if(ModelState.IsValid)
            {
                input.CurriculumPath = UploadMediaPath(input);
                _recruitService.Add(_recruitMapper.MapToEntity(input, new Recruit()));
            }

            return RedirectToAction("Index", "Home");
        }

        private string UploadMediaPath(RecruitViewModel input)
        {
            string path = string.Empty;

            if(input.File != null)
            {
                path = SaveServerFile(input.File, "~/files/Recruit", input.File.FileName);
            }

            return path;
        }
    }
}