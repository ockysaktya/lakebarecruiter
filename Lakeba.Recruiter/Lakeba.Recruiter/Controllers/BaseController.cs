﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lakeba.Recruiter.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string SaveServerFile(HttpPostedFileBase file, string relativePath, string filename = "")
        {
            string pic = string.Empty;

            pic = filename;
            
            string dir = Server.MapPath(relativePath);
            string path = System.IO.Path.Combine(dir, pic);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            file.SaveAs(path);

            return "\\" + path.Replace(Server.MapPath("~"), string.Empty);
        }
	}
}