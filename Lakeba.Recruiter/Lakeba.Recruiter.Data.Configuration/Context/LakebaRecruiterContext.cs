﻿using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Data.Configuration.Mapping;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.Configuration.Context
{
    public class LakebaRecruiterContext : DbContext
    {
        public LakebaRecruiterContext()
            : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.Add(new RecruitMapping());
            modelBuilder.Configurations.Add(new PositionMapping());
        }
    }
}
