namespace Lakeba.Recruiter.Data.Configuration.Migrations
{
    using Contracts.Model.Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Lakeba.Recruiter.Data.Configuration.Context.LakebaRecruiterContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Lakeba.Recruiter.Data.Configuration.Context.LakebaRecruiterContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var positionSet = context.Set<Position>();
            positionSet.AddOrUpdate(x => x.Desc,
                new Position { Desc = "Developer" },
                new Position { Desc = "SeniorDeveloper" },
                new Position { Desc = "Analyst" },
                new Position { Desc = "SystemAnalyst" },
                new Position { Desc = "CEO" });
            
        }
    }
}
