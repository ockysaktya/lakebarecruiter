namespace Lakeba.Recruiter.Data.Configuration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPosition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Positions",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Desc = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Recruits", "PositionId", c => c.Int());
            CreateIndex("dbo.Recruits", "PositionId");
            AddForeignKey("dbo.Recruits", "PositionId", "dbo.Positions", "Id");
            DropColumn("dbo.Recruits", "Position");
       }
        
        public override void Down()
        {
            AddColumn("dbo.Recruits", "Position", c => c.String());
            DropForeignKey("dbo.Recruits", "PositionId", "dbo.Positions");
            DropIndex("dbo.Recruits", new[] { "PositionId" });
            DropColumn("dbo.Recruits", "PositionId");
            DropTable("dbo.Positions");
        }
    }
}
