namespace Lakeba.Recruiter.Data.Configuration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstConfigure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Recruits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Position = c.String(),
                        CurriculumPath = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Recruits");
        }
    }
}
