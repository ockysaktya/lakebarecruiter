﻿recruitApp.controller('recruitController', function ($scope, $http, $modal, recruitService) {
    $scope.recruits = [];

    loadRecruits();

    function loadRecruits() {
        recruitService.getRecruits().then(function () {
            $scope.recruits = recruitService.recruits;
        },
            function () {
                //Error goes here...
            })
            .then(function () {
                $scope.isBusy = false;
            });
    };

    $scope.modalDelete = function (size, recruit) {
        var modalInstance = $modal.open({
            templateUrl: 'app/recruit/html/deleteModal.html',
            controller: function ($scope, $modalInstance,$window, recruit) {
                $scope.candidate = recruit;
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.ok = function (cand) {
                    recruitService.deleteRecruit(cand.Id)
                    .then(function () {
                        $window.location.reload();;
                        $modalInstance.close(cand);
                    },
                    function () {
                        //Error        
                    })
                };
            },
            size: size,
            resolve: {
                recruit: function () {
                    return recruit;
                }
            }
        });
    };

});

recruitApp.controller('recruitAddController', function ($scope, $http, $window, recruitService, positionService) {
        $scope.recruit = {};
        $scope.isEdit = false;
        $scope.positions = [];
        $scope.isBusy = true;

        loadPositions();

        function loadPositions() {
            positionService.getPositions().then(function () {
                $scope.positions = positionService.positions;
            },
                function () {
                    //Error goes here...
                })
                .then(function () {
                    $scope.isBusy = false;
                });
        };

        $scope.cancel = function () {
            $window.location = "#/myrecruits";
        };

        $scope.saveContact = function (file) {

            if ($scope.recruitForm.$invalid) return;
            $scope.recruit.File = file;
            recruitService.addRecruit($scope.recruit)
            .then(function () {
                Quixxi.q.push(['SendEvent', {
                    "event_category": "CandidateAdded",
                    "event_value": "CandidateAdded",
                    "event_detail": "CandidateAdded",
                    "event_object": $scope.recruit
                }]);
                $window.location = "#/myrecruits";
                
            },
            function () {
                //Error        
            })
        };
});

recruitApp.controller('recruitEditController', function ($scope, recruitService, $window, $routeParams, positionService) {
        $scope.recruit = {};
        $scope.isEdit = true;
        $scope.positions = [];

        loadPositions();

        function loadPositions() {
            positionService.getPositions().then(function () {
                $scope.positions = positionService.positions;
            },
                function () {
                    //Error goes here...
                })
                .then(function () {
                    $scope.isBusy = false;
                });
        };

        if ($routeParams.id) {
            $scope.recruit = recruitService.findRecruitById($routeParams.id);
        }

        $scope.cancel = function () {
            $window.location = "#/myrecruits";
        };
    
        $scope.saveContact = function (file) {
            if ($scope.recruitForm.$invalid) return;
            $scope.recruit.File = file;
            recruitService.updateRecruit($scope.recruit)
            .then(function () {
                Quixxi.q.push(['SendEvent', {
                    "event_category": "CandidateEdited",
                    "event_value": "CandidateEdited",
                    "event_detail": "CandidateEdited",
                    "event_object": $scope.recruit
                }]);
                $window.location = "#/myrecruits";
            },
            function () {
                //Error        
            })

        };
    });
