﻿recruitApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: "/app/Home/home.html"
    }),

    $routeProvider.when('/about', {
        templateUrl: "app/Home/about.html"
    }),

    $routeProvider.when('/myrecruits', {
        templateUrl: "app/recruit/html/recruitList.html",
        controller: "recruitController"
    }),

    $routeProvider.when('/myrecruits/new', {
        templateUrl: "app/recruit/html/recruitForm.html",
        controller: "recruitAddController"
    }),

    $routeProvider.when('/myrecruits/:id', {
        templateUrl: "app/recruit/html/recruitForm.html",
        controller: "recruitEditController"
    }),

    $routeProvider.otherwise({
        redirectTo: '/'
    });


}]);