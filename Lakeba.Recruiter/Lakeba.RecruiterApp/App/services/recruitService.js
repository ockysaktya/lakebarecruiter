﻿recruitApp.factory('recruitService', ['$http', '$q','Upload',
function ($http, $q, Upload) {
    var _recruits = [];

    var _getRecruits = function() {
        var deferred = $q.defer();
        var controllerQuery = "api/recruit";

        $http.get(controllerQuery)
            .then(function(result) {
                    // Successful
                    angular.copy(result.data, _recruits);
                    deferred.resolve();
                },
                function(error) {
                    // Error
                    deferred.reject();
                });
        return deferred.promise;
    };

    var _addRecruit = function (recruit) {
        var deferred = $q.defer();
        var controllerQuery = "api/recruit";

        Upload.upload({
            url: controllerQuery,
            data: recruit
        }).then(function (resp) {
            deferred.resolve();
        }, function (resp) {
            // Error
            deferred.reject();
        }, function (evt) {
            
        });

        return deferred.promise;

    };

    var _updateRecruit = function (recruit) {
        var deferred = $q.defer();
        var controllerQuery = "api/recruit/" + recruit.Id;

        Upload.upload({
            url: controllerQuery,
            data: recruit,
            method: 'PUT'
        }).then(function (resp) {
            deferred.resolve();
        }, function (resp) {
            // Error
            deferred.reject();
        }, function (evt) {

        });

        return deferred.promise;
    };

    var _deleteRecruit = function (id) {
        var deferred = $q.defer();
        var controllerQuery = "api/recruit/" + id;

        $http({
            method: 'DELETE',
            url: controllerQuery,
        }).then(function (response) {
            deferred.resolve();
        }, function (error) {
            deferred.reject();
        });
        return deferred.promise;

    };

    function _findRecruitById(id) {
        var found = null;
        $.each(_recruits, function (i, recruit) {
            if (recruit.Id == id) {
                found = recruit;
                return false;
            }
        });
        return found;
    };

    //Expose methods and fields through revealing pattern
    return {
        recruits: _recruits,
        getRecruits: _getRecruits,
        addRecruit: _addRecruit,
        updateRecruit: _updateRecruit,
        deleteRecruit: _deleteRecruit,
        findRecruitById: _findRecruitById
    };

}]);