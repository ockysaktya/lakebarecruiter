﻿recruitApp.factory('positionService', ['$http', '$q',
function ($http, $q) {
    var _positions = [];

    var _getPositions = function() {
        var deferred = $q.defer();
        var controllerQuery = "api/position";

        $http.get(controllerQuery)
            .then(function(result) {
                    // Successful
                    angular.copy(result.data, _positions);
                    deferred.resolve();
                },
                function(error) {
                    // Error
                    deferred.reject();
                });
        return deferred.promise;
    };

    //Expose methods and fields through revealing pattern
    return {
        positions: _positions,
        getPositions : _getPositions
    };

}]);