﻿using System.Web.Optimization;

namespace Lakeba.RecruiterApp.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/angularApp").Include(
                      "~/Scripts/angular.min.js",
                      "~/Scripts/angular-route.min.js",
                      "~/Scripts/angular-resource.min.js",
                      "~/Scripts/angular-ui-bootstrap.js",
                      "~/Scripts/ng-file-upload.min.js",
                      "~/Scripts/angular-block-ui.js",

                      "~/App/recruiteApp.js",
                      "~/App/appRoute.js",
                      "~/App/services/recruitService.js",
                      "~/App/services/positionService.js",
                      "~/App/recruit/controller/recruitController.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                       "~/Content/angular-block-ui.css",
                      "~/Content/site.css"));
        }
    }
}