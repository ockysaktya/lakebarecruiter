﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Lakeba.RecruiterApp.Models.Recruit
{
    public class RecruitViewModel : BaseEntityModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public int? PositionId { get; set; }

        public string CurriculumPath { get; set; }
        public MultipartDataMediaFormatter.Infrastructure.HttpFile File { get; set; }
        public string PositionName { get; set; }
    }
}