﻿namespace Lakeba.RecruiterApp.Models
{
    public class BaseEntityModel
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}