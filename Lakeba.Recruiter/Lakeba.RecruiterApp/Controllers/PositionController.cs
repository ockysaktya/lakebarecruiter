﻿using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Lakeba.RecruiterApp.Models.Position;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lakeba.RecruiterApp.Controllers
{
    public class PositionController : BaseAngularController
    {
        private readonly ICrudBusinessService<Position> _positionService;
        private readonly IMapper<Position,PositionViewModel> _positionMapper;

        public PositionController(ICrudBusinessService<Position> positionService, IMapper<Position, PositionViewModel> positionMapper)
        {
            _positionService = positionService;
            _positionMapper = positionMapper;
        }

        // GET api/<controller>
        public IEnumerable<PositionViewModel> Get()
        {
            var recruits = _positionService.GetAll();
            return recruits.Select(x => _positionMapper.MapToInput(x));
        }
    }
}
