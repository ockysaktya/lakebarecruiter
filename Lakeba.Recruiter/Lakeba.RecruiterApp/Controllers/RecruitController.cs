﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Lakeba.Recruiter.Framework.Injectors;
using Lakeba.RecruiterApp.Models.Recruit;
using System.Web;
using System.Web.Hosting;
using System.IO;

namespace Lakeba.RecruiterApp.Controllers
{
    public class RecruitController : BaseAngularController
    {
        private readonly IRecruitBusinessService _recruitService;
        private readonly IMapper<Recruit, RecruitViewModel> _recruitMapper;

        private void ResolveFileUpload(RecruitViewModel model)
        {
           var file = HttpContext.Current.Request.Files.Count > 0 ?
                                HttpContext.Current.Request.Files[0] : null;

            if (file != null && file.ContentLength > 0)
            {
                var path = SaveServerFile(model.File, "~/files/Recruit", model.File.FileName);
                model.CurriculumPath = path;
            }
        }

        public RecruitController()
        {
            _recruitService = IoC.Resolve<IRecruitBusinessService>();
            _recruitMapper = IoC.Resolve<IMapper<Recruit, RecruitViewModel>>();
        }


        // GET api/<controller>
        public IEnumerable<RecruitViewModel> Get()
        {
            var recruits = _recruitService.GetAll();
            return recruits.Select(x => _recruitMapper.MapToInput(x));
        }

        // GET api/<controller>/5
        public RecruitViewModel Get(int id)
        {
            var ety = _recruitService.Get(id);
            return _recruitMapper.MapToInput(ety);
        }

        // POST api/<controller>
        public void Post(RecruitViewModel model)
        {
            ResolveFileUpload(model);
            _recruitService.Add(_recruitMapper.MapToEntity(model, new Recruit()));
        }

        // PUT api/<controller>/5
        public void Put(int id, RecruitViewModel model)
        {
            ResolveFileUpload(model);
            var ety = _recruitService.Get(id);
            var modified = _recruitMapper.MapToEntity(model, ety);
            _recruitService.Update(modified);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var ety = _recruitService.Get(id);
            ety.IsDeleted = true;
            _recruitService.Update(ety);
        }
    }
}