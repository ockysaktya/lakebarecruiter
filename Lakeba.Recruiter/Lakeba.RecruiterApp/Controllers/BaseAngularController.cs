﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Lakeba.Recruiter.Framework.Injectors;
using Lakeba.RecruiterApp.Models.Recruit;
using System.Web;
using System.Web.Hosting;
using System.IO;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using MultipartDataMediaFormatter.Infrastructure;

namespace Lakeba.RecruiterApp.Controllers
{
    public abstract class BaseAngularController : ApiController
    {
        protected ActionResult Json(object data, JsonRequestBehavior behavior)
        {
            var jsonSerializerSetting = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            if ( behavior == JsonRequestBehavior.DenyGet)
            {
                throw new InvalidOperationException("GET is not permitted for this request.");
            }
            var jsonResult = new ContentResult
            {
                Content = JsonConvert.SerializeObject(data, jsonSerializerSetting),
                ContentType = "application/json"
            };
            return jsonResult;

        }

        protected string SaveServerFile(HttpFile file, string relativePath, string filename = "")
        {
            string pic = string.Empty;

            pic = filename;

            string dir = HostingEnvironment.MapPath(relativePath);
            string path = System.IO.Path.Combine(dir, pic);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            File.WriteAllBytes(path, file.Buffer);

            return "\\" + path.Replace(HostingEnvironment.MapPath("~"), string.Empty);
        }
    }
}