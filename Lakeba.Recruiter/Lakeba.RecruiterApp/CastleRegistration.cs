﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lakeba.Recruiter.Framework.Injectors;
using Lakeba.Recruiter.Framework.UI;

namespace Lakeba.RecruiterApp
{
    public class CastleRegistration
    {
        public static void Register()
        {
            // the order matters, DO NOT CHANGE the order if you don't understand it
            WindsorRegistrator.Register(typeof(IPageHeadBuilder), typeof(PageHeadBuilder));

            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.Contracts.Model");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.Data.Configuration");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.Contracts.DataContract");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.Contracts.BusinessContract");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.Data.DataService");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.Recruiter.BusinessService");
            WindsorRegistrator.RegisterApiController("Lakeba.RecruiterApp");
            WindsorRegistrator.RegisterAllFromAssemblies("Lakeba.RecruiterApp");
        }
    }
}