﻿using Lakeba.Recruiter.Contracts.BusinessContract.ModelMapper;
using Lakeba.Recruiter.Contracts.Model.Domain;
using Lakeba.Recruiter.Contracts.Model.ModelMapper;
using Lakeba.RecruiterApp.Models.Recruit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lakeba.RecruiterApp.Infrastructures.Mappers
{
    public class RecruiterMapper : Mapper<Recruit,RecruitViewModel>, IMapper<Recruit, RecruitViewModel>
    {
        public override RecruitViewModel MapToInput(Recruit entity)
        {
            var converted = base.MapToInput(entity);
            
            converted.PositionName = entity.Position != null ? entity.Position.Desc : string.Empty;
            return converted;
        }
    }
}