﻿using Lakeba.Recruiter.Contracts.DataContract.Services;
using Lakeba.Recruiter.Contracts.DataContract.UnitOfWork;
using Lakeba.Recruiter.Contracts.Model.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.DataService.Services
{
    public class RecruitDataService : LakebaRecruiterDataService<Recruit> , IRecruitDataService
    {
        public RecruitDataService(ILakebaRecruiterUnitOfWork service)
            :base(service)
        {
        }
    }
}
