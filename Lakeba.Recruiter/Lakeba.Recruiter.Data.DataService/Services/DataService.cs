﻿using Lakeba.Recruiter.Contracts.DataContract.Services;
using Lakeba.Recruiter.Contracts.DataContract.Services.Repository;
using Lakeba.Recruiter.Contracts.DataContract.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.DataService.Services
{
    public class DataService<TEntity> : IDataService<TEntity> where TEntity : class , new()
    {
        public IUnitOfWork UnitOfWork { get; set; }
        protected IRepository<TEntity> Repository { get; set; }

        public DataService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            Repository = UnitOfWork.Repository<TEntity>();
        }

        public IEnumerable<TEntity> DbSet()
        {
            return Repository.GetAll();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Find(predicate);
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Where(predicate);
        }

        public TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Single(predicate);
        }

        public TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.First(predicate);
        }

        public IEnumerable<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            return Repository.Include<TProperty>(path);
        }

        public IEnumerable<TEntity> Include(string path)
        {
            return Repository.Include(path);
        }

        public virtual void Add(TEntity entity)
        {
            Repository.Add(entity);
        }

        public void Attach(TEntity entity)
        {
            Repository.Attach(entity);
        }

        public void Update(TEntity entity, int id)
        {
            Repository.Update(entity, id);
        }

        public void Delete(TEntity entity)
        {
            Repository.Remove(entity);
        }

        public void Save()
        {
            UnitOfWork.Save();
        }

        public int Truncate()
        {
            return Repository.Truncate();
        }
    }
}
