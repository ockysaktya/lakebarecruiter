﻿using Lakeba.Recruiter.Contracts.DataContract.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.DataService.Services
{
    public class LakebaRecruiterDataService<TEntity> : DataService<TEntity> where TEntity : class , new()
    {
        public LakebaRecruiterDataService(ILakebaRecruiterUnitOfWork unitOfWork)
            :base(unitOfWork)
        {
        }
    }
}
