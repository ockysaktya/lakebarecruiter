﻿using Lakeba.Recruiter.Contracts.DataContract.UnitOfWork;
using Lakeba.Recruiter.Data.Configuration.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.DataService.UnitOfWork
{
    public class LakebaRecruiterUnitOfWork : UnitOfWork , ILakebaRecruiterUnitOfWork
    {
        public LakebaRecruiterUnitOfWork()
            : base(new LakebaRecruiterContext())
        {
        }
    }
}
