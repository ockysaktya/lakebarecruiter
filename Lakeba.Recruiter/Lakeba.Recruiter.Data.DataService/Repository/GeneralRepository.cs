﻿using Lakeba.Recruiter.Contracts.DataContract.Services.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.Data.DataService.Repository
{
    public class GeneralRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GeneralRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        private string GetTableName()
        {
            var type = typeof(TEntity);
            var entityName = (_context as System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext.CreateObjectSet<TEntity>().EntitySet.Name;
            var tableAttribute = type.GetCustomAttributes(false).OfType<TableAttribute>().FirstOrDefault();

            return tableAttribute == null ? entityName : tableAttribute.Name;
        }

        public int Truncate()
        {
            return _context.Database.ExecuteSqlCommand("TRUNCATE TABLE [" + GetTableName() + "]");
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        public IEnumerable<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            return _context.Set<TEntity>().Include(path);
        }

        public IEnumerable<TEntity> Include(string path)
        {
            return _context.Set<TEntity>().Include(path);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        public TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.SingleOrDefault(predicate);
        }

        public TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _dbSet.Add(entity);
        }

        public void Update(TEntity entity, int id)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            var entry = _context.Entry(entity);

            if (entry.State == EntityState.Detached)
            {
                var set = _context.Set<TEntity>();
                TEntity attachedEntity = set.Find(id);  // You need to have access to key

                if (attachedEntity != null)
                {
                    var attachedEntry = _context.Entry(attachedEntity);
                    attachedEntry.CurrentValues.SetValues(entity);
                }
                else
                {
                    entry.State = EntityState.Modified; // This should attach entity
                }
            }
        }

        public void Attach(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _dbSet.Attach(entity);
        }

        public void AttachAsModified(TEntity entity)
        {
            var entry = _context.Entry(entity);

            entry.State = EntityState.Modified;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }
    }
}
