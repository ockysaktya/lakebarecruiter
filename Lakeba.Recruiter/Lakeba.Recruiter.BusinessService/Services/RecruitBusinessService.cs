﻿using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.DataContract.Services;
using Lakeba.Recruiter.Contracts.Model.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.BusinessService.Services
{
    public class RecruitBusinessService : CrudBusinessService<Recruit> , IRecruitBusinessService
    {
        public RecruitBusinessService(IDataService<Recruit> service)
            :base(service)
        {
        }
    }
}
