﻿using Lakeba.Recruiter.Contracts.BusinessContract.Services;
using Lakeba.Recruiter.Contracts.DataContract.Services;
using Lakeba.Recruiter.Contracts.Model.Abstract;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.Recruiter.BusinessService.Services
{
    public class CrudBusinessService<T> : ICrudBusinessService<T> where T : Entity,new()
    {
        protected readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected IDataService<T> Service { get; set; }

        public CrudBusinessService(IDataService<T> servive)
        {
            Service = servive;
        }

        public T Get(int id)
        {
            return Service.Single(ety => ety.Id == id);
        }

        public T Get(Expression<Func<T, bool>> func)
        {
            return Service.Single(func);
        }

        public IEnumerable<T> GetAll()
        {
            return Service.GetAll().Where(ety => !ety.IsDeleted);
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> func, bool showDeleted = false)
        {
            return Service.Where(func).Where(ety => !ety.IsDeleted);
        }

        public virtual int Add(T item)
        {
            try
            {
                Service.Add(item);
                Service.Save();
                return item.Id;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public virtual int Update(T item)
        {
            try
            {
                Service.Update(item, item.Id);
                Service.Save();
                return item.Id;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void Save()
        {
            try
            {
                Service.Save();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void Delete(int id)
        {
            try
            {
                var entity = Get(id);
                entity.IsDeleted = true;
                Save();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void HardDelete(int id)
        {
            try
            {
                var entity = Get(id);
                Service.Delete(entity);
                Save();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }

        public void Restore(int id)
        {
            throw new NotImplementedException();
        }

        public int Truncate()
        {
            try
            {
                return Service.Truncate();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }

        }
    }
}
