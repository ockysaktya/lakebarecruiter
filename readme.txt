Front End:
-. Bootstrap3
-. AngularJs with standard and widely used modules/directives such as: AngularUi, AngularRoute, Upload, BlockUi, etc
-. Centralized angularJs artifacts for easy maintenance and understanding, you can find everything under "App" folder so don't need to looks at another place.
-. File divided base on "Separation  of concern" principles

Back End:
- Entity Framework Code First, complete with the migration and seed method and connect to SQL SERVER
- Asp.Net Web API 
- Castle Windsor dependency injector for Library/Assembly resolver and Controller resolver
- N-tier architecture (Data Layer,Business Layer, and Presentation Layer), following the SOA principles
- Repository, UnitOfWork and Mapper/Adapter Design Pattern
- Code infrastructures with SOLID principles in mind